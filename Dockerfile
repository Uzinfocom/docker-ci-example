# base image
FROM google/debian:wheezy

# copy all files from current dir to container dir
COPY docker-ci-example /

WORKDIR /

# expose port for service
EXPOSE 8000

# init command to run
CMD ["./main"]
