package person

import "testing"

// test person age correctness
func TestSetAge(t *testing.T) {
	// prepare table data with correct and wrong args
	testInput := map[int]error{
		0:   nil,
		10:  nil,
		-10: InvalidAge,
		100: nil,
		170: InvalidAge,
	}

	var p Self
	for k, v := range testInput {
		err := p.SetAge(k)
		if err != v {
			t.Errorf("got %v, want %v with arg %s", err, v, k)
		}
	}

}

// test person name correctness
func TestSetName(t *testing.T) {
	// prepare table data
	testInput := map[string]error{
		"":     InvalidName,
		"Mike": nil,
		"VeryLongNameWhichShouldNotValidForTest": InvalidName,
		"JohnSebastianBah":                       nil,
	}

	var p Self
	for k, v := range testInput {
		err := p.SetName(k)
		if err != v {
			t.Errorf("got %v, want %v with arg %s", err, v, k)
		}
	}

}
